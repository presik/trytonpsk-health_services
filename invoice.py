#  This file is part of Tryton.  The COPYRIGHT file at the top level of
#  this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'
    patient = fields.Many2One('health.patient', 'Patient',
        states={'readonly': Eval('state') != 'draft'},
    )
    insurance_plan = fields.Many2One('health.insurance.plan',
        'Insurance Plan', states={'readonly': True})


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @classmethod
    def _get_origin(cls):
        return super(InvoiceLine, cls)._get_origin() + [
            'health.service.line', 'health.service']
