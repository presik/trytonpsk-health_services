from datetime import datetime, date

from trytond.i18n import gettext
from trytond.exceptions import UserError
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateTransition, StateView, Button
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.pyson import Eval


class CreateServiceInvoiceInit(ModelView):
    'Create Service Invoice Init'
    __name__ = 'health.service.invoice.init'
    payment_method = fields.Many2One('account.invoice.payment.method',
        "Payment Method", required=True)
    payment_amount = fields.Numeric('Payment amount', required=True,
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    currency_digits = fields.Integer('Currency Digits')
    party = fields.Many2One('party.party', 'Party', readonly=True)

    @classmethod
    def default_require_voucher(cls):
        return False

    @classmethod
    def default_do_invoice(cls):
        return True


class CreateServiceInvoice(Wizard):
    'Create Service Invoice'
    __name__ = 'health.service.invoice.create'

    start = StateView(
        'health.service.invoice.init',
        'health_services.view_health_service_invoice', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create Invoice', 'create_invoice', 'tryton-ok', True),
        ])
    create_invoice = StateTransition()

    def default_start(self, fields):
        pool = Pool()
        Service = pool.get('health.service')
        service = Service(Transaction().context['active_id'])
        return {
            'payment_amount': service.total_amount,
            'currency_digits': 2,
            'party': service.patient.id,
        }

    @classmethod
    def __setup__(cls):
        super(CreateServiceInvoice, cls).__setup__()

    def get_invoice_line(self, line, unit_price, seq):
        taxes = []
        if line.invoice_line:
            return
        # Include taxes related to the product on the invoice line
        for product_tax_line in line.product.customer_taxes_used:
            taxes.append(product_tax_line.id)
        res = {
            'origin': str(line),
            'product': line.product.id,
            'description': line.service.number + ' | ' + line.desc,
            'quantity': line.qty,
            'account': line.product.template.account_revenue_used.id,
            'unit': line.product.default_uom.id,
            'unit_price': unit_price,
            'sequence': seq,
            'taxes': [('add', taxes)],
        }
        return res

    def get_invoice_data(self, party, service):
        invoice_data = {}
        invoice_data['description'] = service.desc
        invoice_data['party'] = party.id
        invoice_data['type'] = 'out'
        invoice_data['patient'] = service.patient.id
        invoice_data['invoice_date'] = date.today()
        if not party.account_receivable:
            raise UserError(
                gettext('health_services.msg_no_account_receivable'))
        invoice_data['account'] = party.account_receivable.id
        return invoice_data

    def transition_create_service_invoice(self):
        pool = Pool()
        Service = pool.get('health.service')
        Invoice = pool.get('account.invoice')
        Party = pool.get('party.party')
        Journal = pool.get('account.journal')
        MoveLine = pool.get('account.move.line')

        currency_id = Transaction().context.get('currency')
        services = Service.browse(Transaction().context.get('active_ids'))
        invoices = []

        # Invoice Header
        for service in services:
            if service.state == 'invoiced':
                raise UserError(gettext(
                    'health_services.msg_duplicate_invoice'))
            if service.invoice_to:
                party = service.invoice_to
            else:
                party = service.patient.party

            invoice_data = self.get_invoice_data(party, service)
            ctx = {}
            sale_price_list = None
            if hasattr(party, 'sale_price_list'):
                sale_price_list = party.sale_price_list

            if sale_price_list:
                ctx['price_list'] = sale_price_list.id
                ctx['sale_date'] = date.today()
                ctx['currency'] = currency_id
                ctx['customer'] = party.id

            journals = Journal.search([
                ('type', '=', 'revenue'),
                ], limit=1)

            if journals:
                journal, = journals
            else:
                journal = None

            invoice_data['journal'] = journal.id
            party_address = Party.address_get(party, type='invoice')
            if not party_address:
                raise UserError(gettext(
                    'health_services.msg_no_invoice_address'))
            invoice_data['invoice_address'] = party_address.id
            invoice_data['reference'] = service.number

            if not party.customer_payment_term:
                raise UserError(gettext('health_services.msg_no_payment_term'))

            invoice_data['payment_term'] = party.customer_payment_term.id

            # Invoice Lines
            seq = 0
            invoice_lines = []
            for line in service.lines:
                seq = seq + 1
                if sale_price_list:
                    with Transaction().set_context(ctx):
                        unit_price = sale_price_list.compute(
                            party, line.product, line.product.list_price,
                            line.qty, line.product.default_uom
                        )
                else:
                    unit_price = line.product.list_price

                if line.to_invoice:
                    invoice_line = self.get_invoice_line(line, unit_price, seq)
                    if not invoice_line:
                        continue
                    invoice_lines.append(('create', [invoice_line]))
                invoice_data['lines'] = invoice_lines

            invoices.append(invoice_data)

        new_invoices = Invoice.create(invoices)
        Invoice.update_taxes(new_invoices)
        invoice = new_invoices[0]
        today = date.today()
        to_reconcile = []
        for line in invoice.lines:
            line.origin.invoice_line = line.id
            line.origin.save()
        amount = self.start.payment_amount
        payment_method = self.start.payment_method
        Invoice.post([invoice])
        if self.start.payment_method and invoice.total_amount == amount:
            to_reconcile, remainder = invoice.get_reconcile_lines_for_amount(amount)
            to_reconcile = list(to_reconcile)
            lines = invoice.pay_invoice(amount, payment_method, today)
            to_reconcile += lines
            if to_reconcile:
                MoveLine.reconcile(to_reconcile)

        # Change to invoiced the status on the service document.
        Service.write(services, {'state': 'invoiced'})

        return 'end'
